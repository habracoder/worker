<?php

namespace App\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\UserBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser
{
    const
        ROLE_ADMIN    =    'ROLE_ADMIN',
        ROLE_USER     =    'ROLE_USER',
        ROLE_FACEBOOK =    'ROLE_FACEBOOK';

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @var string
     * @Assert\Regex(
     *     pattern="/^\s*[a-z0-9\_\.]{6,16}\s*$/",
     *     match=true,
     *     message="Use only latin chars (a-z), digits (0-9) and special chars like '.', '_'. From 6 to 16 chars."
     * )
     */
    protected $username;

    /**
     * @ORM\OneToOne(targetEntity="App\UserBundle\Entity\Profile", mappedBy="user", cascade={"persist"})
     */
    private $profile;

    /**
     * @Assert\Length(
     *      min = "6",
     *      max = "12",
     *      minMessage = "Password must be at least {{ limit }} characters length",
     *      maxMessage = "Password cannot be longer than {{ limit }} characters length"
     * )
     */
    protected $plainPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="integer", length=20)
     */
    protected $facebookId;

    public function __construct()
    {
        parent::__construct();

        if (null == $this->getUsername()) {
            $this->setUsername(uniqid('u_'));
        }

        $this->groups = new ArrayCollection();
        $this->agents = new ArrayCollection();
    }

    public function addRole($role)
    {
        $role = strtoupper($role);

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole($role)
    {
        $this->roles = array_diff($this->roles, [$role]);

        return $this;
    }

    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * Get user's roles
     * @return array roles
     */
    public function getRoles()
    {
        return array_unique($this->roles);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile
     *
     * @param Profile $profile
     *
     * @return User
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }
    
    /**
     * It needs for facebook integration
     */
    public function serialize()
    {
        return serialize(array($this->facebookId, parent::serialize()));
    }
    
    /**
     * It needs for facebook integration
     */
    public function unserialize($data)
    {
        list($this->facebookId, $parentData) = unserialize($data);
        parent::unserialize($parentData);
    }
    
    /**
     * @param string $facebookId
     * @return void
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        $this->setUsername($facebookId);
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }
    
    /**
     * It needs for facebook integration
     * @param Array
     */
    public function setFBData($fbdata)
    {
        if (isset($fbdata['id'])) {
            $this->setFacebookId($fbdata['id']);
            $this->addRole('ROLE_FACEBOOK');
        }
        
        if (isset($fbdata['first_name'])) {
            $this->setFirstname($fbdata['first_name']);
        }
        
        if (isset($fbdata['last_name'])) {
            $this->setLastname($fbdata['last_name']);
        }
        
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
    }
}