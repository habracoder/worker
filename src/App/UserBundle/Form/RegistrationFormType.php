<?php

namespace App\UserBundle\Form;

use App\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('profile', new ProfileType(), array(
            'label' => false
        ));

        $builder->add(
            'email',
            'email',
            array(
                'label' => 'property.email'
            )
        );

        $builder->remove('username');

        $builder->add(
        'plainPassword',
        'repeated',
        array(
            'type' => 'password',
            'first_options' => array('label' => 'property.new_password'),
            'second_options' => array('label' => 'property.confirm_password'),
            'invalid_message' => 'form.validate.match_password',
            'required' => false,
        )
    );
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}