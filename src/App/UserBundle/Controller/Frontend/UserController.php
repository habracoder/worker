<?php

namespace App\UserBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package App\UserBundle\Controller\Frontend
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_list")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $er = $em->getRepository('UserBundle:User');

        return [
            'users' => $er->findBy([
                'enabled' => true
            ], [
                'id' => 'desc'
            ])
        ];
    }
}
