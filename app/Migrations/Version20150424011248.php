<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150424011248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS `user` (
                `id`                    INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `username`              VARCHAR(255) NOT NULL,
                `username_canonical`    VARCHAR(255) NOT NULL,
                `email`                 VARCHAR(255) NOT NULL,
                `email_canonical`       VARCHAR(255) NOT NULL,
                `enabled`               TINYINT(1) NOT NULL,
                `salt`                  VARCHAR(255) NOT NULL,
                `password`              VARCHAR(255) NOT NULL,
                `last_login`            DATETIME DEFAULT NULL,
                `locked`                TINYINT(1) NOT NULL,
                `expired`               TINYINT(1) NOT NULL,
                `expires_at`            DATETIME DEFAULT NULL,
                `confirmation_token`    VARCHAR(255) DEFAULT NULL,
                `password_requested_at` DATETIME DEFAULT NULL,
                `roles`                 LONGTEXT NOT NULL COMMENT '(DC2Type:array)',
                `credentials_expired`   TINYINT(1) NOT NULL,
                `credentials_expire_at` DATETIME DEFAULT NULL,
                UNIQUE INDEX `user_UI_username_canonical` (`username_canonical`),
                UNIQUE INDEX `user_UI_email_canonical` (`email_canonical`),
                PRIMARY KEY (`id`)
            )  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB
        ");

        $this->addSql("
            CREATE TABLE IF NOT EXISTS `user_group` (
                `id`    INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `name`  VARCHAR(255) NOT NULL,
                `roles` LONGTEXT NOT NULL COMMENT '(DC2Type:array)',
                UNIQUE INDEX `user_group_UI_name` (`name`),
                PRIMARY KEY (`id`)
            )  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB
        ");

        $this->addSql("
            CREATE TABLE IF NOT EXISTS `user_to_group` (
                `user_id`  INT(11) UNSIGNED NOT NULL,
                `group_id` INT(11) UNSIGNED NOT NULL,
                INDEX `user_to_group_UI_user_id` (`user_id`),
                INDEX `user_to_group_UI_group_id` (`group_id`),
                PRIMARY KEY (`user_id`, `group_id`)
            )  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB
        ");

        $this->addSql("
            ALTER TABLE `user_to_group`
                ADD CONSTRAINT `user_to_group_FK_user_id`
                    FOREIGN KEY (`user_id`)
                        REFERENCES `user` (`id`)
        ");

        $this->addSql("
            ALTER TABLE `user_to_group`
                ADD CONSTRAINT `user_to_group_FK_group_id`
                    FOREIGN KEY (`group_id`)
                        REFERENCES `user_group` (`id`)
        ");

        $this->addSql("
        CREATE TABLE IF NOT EXISTS `profile` (
            `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `user_id`     INT(11) UNSIGNED NOT NULL,
            `first_name`  VARCHAR(128) NOT NULL,
            `last_name`   VARCHAR(128) NOT NULL,
            UNIQUE `profile_UI_user_id` (`user_id`),
            PRIMARY KEY(`id`)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE = InnoDB;
        ");

        $this->addSql("
            ALTER TABLE `profile`
                ADD CONSTRAINT `profile_FK_user_id`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`)
                  ON DELETE CASCADE
                  ON UPDATE CASCADE;
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            ALTER TABLE `profile` DROP FOREIGN KEY `profile_FK_user_id`
        ");

        $this->addSql("
            ALTER TABLE `user_to_group`
                DROP FOREIGN KEY `user_to_group_FK_group_id`,
                DROP FOREIGN KEY `user_to_group_FK_user_id`
        ");

        $this->addSql("DROP TABLE IF EXISTS `profile`");
        $this->addSql("DROP TABLE IF EXISTS `user_to_group`");
        $this->addSql("DROP TABLE IF EXISTS `user_group`");
        $this->addSql("DROP TABLE IF EXISTS `user`");
    }
}
