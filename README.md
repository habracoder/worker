# Работник
@version 1.0

Проект состоиться из основных технологий:
  - Symfony2
  - MySQL
  - Doctrine2
  - Memcache

Для начала необходимо подтянуть проэкт
```sh
$ git clone git@github.com:habracoder/worker.git
```

> Для выполнения необходимо предустановленное расширение GIT

После удачной загрузки проэкта, необходимо создать 
файл параметров `parameters.yml`, можно использовать заготовку
`parameters.yml.dist`.

После этого необходимо настроить права выполнения для папок кеш и логи
подробней описано на http://symfony.com/doc/current/book/installation.html

Затем, Вам необходимо поднять базу
```sh
$ cd worker
$ php app/console --no-debug doctrine:database:create
$ php app/console --no-debug doctrine:migration:migrate
```